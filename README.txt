TestPerformance.java:
1. Which of the two lists performs better as size increases?
    The ArrayList performs better, with a much lower runtime than the LinkedList.

TestIterator.java:
1. Try with LinkedList- does it make any difference behaviorally?
    Using a LinkedList makes the program operate more slowly.
2. What happens if you use list.remove(Integer.valueOf(77))?
    This is slower than just using i.remove(); for that operation.


TestList.java:
1. What does this (remove) method do?
    This method removes a value at a given index.
2. What does this one (remove(value)) do?
    This method removes a certain value from the list at its first found index.